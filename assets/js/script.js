$(document).ready(function() {

  // Shows current year in footer //////////////////////////////////////////////
  $(".legal span").text((new Date()).getFullYear());


  // About page team slider ////////////////////////////////////////////////////
  $('.team-slider').slick({
    arrows: false,
    adaptiveHeight: true,
    asNavFor: '.team-slider-nav',
    infinite: false,
    cssEase: 'cubic-bezier(.6,.24,.02,.99)'
  });
  $('.team-slider-nav').slick({
    arrows: true,
    adaptiveHeight: true,
    asNavFor: '.team-slider',
    vertical: true,
    prevArrow: '<span class="slick-prev"><img src="assets/img/arrow-left.svg" alt="Strelytė į kairę"></span>',
    nextArrow: '<span class="slick-next"><img src="assets/img/arrow-right.svg" alt="Strelytė į dešinę"></span>',
    infinite: false,
    cssEase: 'cubic-bezier(.6,.24,.02,.99)'
  });

  // Loading animations ////////////////////////////////////////////////////////
  $("header nav ul li a, .btn, footer .social-links ul li a, header .logo, ul#works-menu li a, section#about .team-slider-container .team-slider-nav .slick-arrow, #capabilities-nav ul li a, #capabilities-nav .capabilities-nav-arrows .nav-list-arrow, section#works .img-box").mouseenter(function() {
    if (!$('.mouse-dot').hasClass('hover')) {
      $('.mouse-dot').addClass('hover');
    }
  });

  $("header nav ul li a, .btn, footer .social-links ul li a, header .logo, ul#works-menu li a, section#about .team-slider-container .team-slider-nav .slick-arrow, #capabilities-nav ul li a, #capabilities-nav .capabilities-nav-arrows .nav-list-arrow, section#works .img-box").mouseleave(function() {
    if ($('.mouse-dot').hasClass('hover')) {
      $('.mouse-dot').removeClass('hover');
    }
  });

  $("header nav ul li a, .btn, footer .social-links ul li a, header .logo, ul#works-menu li a, section#about .team-slider-container .team-slider-nav .slick-arrow, #capabilities-nav ul li a, #capabilities-nav .capabilities-nav-arrows .nav-list-arrow, section#works .img-box").mousedown(function() {
    if (!$('.mouse-dot').hasClass('down')) {
      $('.mouse-dot').addClass('down');
    }
  });

  $("header nav ul li a, .btn, footer .social-links ul li a, header .logo, ul#works-menu li a, section#about .team-slider-container .team-slider-nav .slick-arrow, #capabilities-nav ul li a, #capabilities-nav .capabilities-nav-arrows .nav-list-arrow, section#works .img-box").mouseup(function() {
    if ($('.mouse-dot').hasClass('down')) {
      $('.mouse-dot').removeClass('down');
    }
  });

  $('section#cta').mouseenter(function() {
    $('.mouse-dot').addClass('inverse');
  });
  $('section#cta').mouseleave(function() {
    $('.mouse-dot').removeClass('inverse');
  });



  // Page items animations /////////////////////////////////////////////////////
  $(".animation-item").each(function() {
    $(this).add('.btn.btn--index, .gradient-box--animation, .contacts-list li span, .contacts-list li a, section#about .team-slider-container').addClass('animation-start');
  });
  $('#capabilities-nav ul, #capabilities-nav .capabilities-nav-arrows').addClass('animation-start');

  $(window).scroll(function() {
    inViewport();
  });

  $(window).resize(function() {
    inViewport();
  });

  function inViewport() {
    $('.animation-scroll').each(function() {
      var divPos = $(this).offset().top,
        topOfWindow = $(window).scrollTop(),
        mainHeight = $('main').height(),
        windowHeight = $(window).height();

      // console.log(windowHeight);
      if (divPos < topOfWindow + 600) {
        $(this).addClass('animation-start');
      }
      if (mainHeight < topOfWindow + windowHeight / 2 + 100) {
        $('section#cta .animation-item--cta, section#cta .btn').addClass('animation-start');
      }
    });
  }

  $('#burger-menu-btn').click(function() {
    $(this).toggleClass('burger-menu-btn--open');
    $('nav.nav--mobile, .page-dim').toggleClass('mobile-nav--open');
  });
  $('.page-dim').click(function() {
    $(this).add('nav.nav--mobile').removeClass('mobile-nav--open');
    $('#burger-menu-btn').removeClass('burger-menu-btn--open');
  });


	//  Bind the event handler to the "submit" JavaScript event
$('form').submit(function () {

    // Get the Login Name value and trim it
    var name = $.trim($('#input-name').val());
    var email = $.trim($('#input-email').val());
    var phone = $.trim($('#input-phone').val());

    // Check if empty of not
    if (name === '') {
			console.log($(this));
				$(this).addClass('error-input')
				$(this).closest('label').append('<span class="error-input">*</span>');
				return false;
    }
});

$("#scroll-to-form").click(function() {
    $([document.documentElement, document.body]).animate({
      scrollTop: $("#form").offset().top
    }, 900);
  });

});


// Background mouse move animation ///////////////////////////////////////////
function parallaxIt(e, target, movement) {
  var WindowWidth = $(window).width();
  if (WindowWidth > 1024) {
    var $this = $("#page-container");
    var relX = e.pageX - $this.offset().left;

    TweenMax.to(target, 1, {
      x: (relX - $this.width() / 2) / $this.width() * movement,
    });
  }
}

$(document).ready(function() {
  $("#page-container").mousemove(function(e) {
    parallaxIt(e, ".main-lines span", -50);
  });
});

$(window).resize(function() {
  $("#page-container").mousemove(function(e) {
    parallaxIt(e, ".main-lines span", -50);
  });
});

// Mouse dot /////////////////////////////////////////////////////////////////


function Mouse(e, target, movement) {
  var WindowWidth = $(window).width();
  if (WindowWidth > 1024) {

    if (!$('.mouse-dot').length) {
      $('#page-container').append('<div class="mouse-dot"></div>');
    }

    var
      $body = $('body'),
      windowWidth = $body.width(),
      windowHeight = $(window).height();

    $(document).on('mousemove', function(e) {

      $('.mouse-dot').css({
        left: e.pageX,
        top: e.pageY - window.scrollY,
      });
      if ((e.pageX > windowWidth - 20) || (e.pageY - window.scrollY > windowHeight - 20) || (e.pageX < 20) || (e.pageY < 20)) {
        if (!$('.mouse-dot').hasClass('border')) {
          $('.mouse-dot').addClass('border');
        }
      } else {
        if ($('.mouse-dot').hasClass('border')) {
          $('.mouse-dot').removeClass('border');
        }
      }
    });
  }
}

$(document).ready(function() {
  Mouse();
});

$(window).resize(function() {
  Mouse();
});

// $(window).scroll(function(){
// 	Mouse();
// });
